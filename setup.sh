#!/usr/bin/env bash

# Global setup script for Castor-data

cd embeddings/GloVe
echo 'Processing GloVe'
echo '==========================================='
if [ -f glove.840B.300d.txt ]; then
    echo 'glove.840B.300d.txt already exists.. skipping glove.840B.300d.zip extraction..'
else
    unzip glove.840B.300d.zip
fi
cd - > /dev/null

echo ''
cd datasets/TrecQA
echo 'Processing TrecQA'
echo '==========================================='
python parse.py
python overlap_features.py
python build_vocab.py
python build_qrels.py
cd - > /dev/null

echo ''
cd datasets/WikiQA
echo 'Processing WikiQA'
echo '==========================================='
if [ -d WikiQACorpus ]; then
    echo 'WikiQACorpus/ already exists.. skipping WikiQACorpus.zip extraction..'
else
    unzip WikiQACorpus.zip
fi
python create-train-dev-test-data.py
cd - > /dev/null
