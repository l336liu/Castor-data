import glob
import os
import random
import re

def cut_length(sent):
	if len(sent.split(" ")) > 48:
		return False
	else:
		return True

def extract_file_name(dir_name):
	# eg: STS.input.OnWN.txt / STS.gs.OnWN.txt
	files = glob.glob(os.path.join(dir_name, "STS.input.*.txt"))
	reg = re.compile('input.(.*?).txt',re.S)
	files = [reg.findall(ele)[0] for ele in files]
	input_file = []
	sim_file = []
	for uname in files:
		input_file.append(os.path.join(dir_name, "STS.input." + uname + ".txt"))
		sim_file.append(os.path.join(dir_name, "STS.gs." + uname + ".txt"))

	return input_file, sim_file


def dump(folder, data):
        print(folder)
        f_id = open(os.path.join(folder, "id.txt"), "a+")
        f_q1 = open(os.path.join(folder, "a.toks"), "a+")
        f_q2 = open(os.path.join(folder, "b.toks"), "a+")
        f_label = open(os.path.join(folder, "sim.txt"), "a+")
        
        for item in data:
                f_id.write(item[0] + "\n")
                f_q1.write(item[1] + "\n")
                f_q2.write(item[2] + "\n")
                f_label.write(item[3])

def gen_train_pairs(input_file, sim_file, idx):
	f_input = open(input_file).readlines()
	f_label = open(sim_file).readlines()	
	output_data = []

	for i in range(len(f_input)):
		line = f_input[i]
		sim = f_label[i]
		sent_a, sent_b = line.rstrip("\n").split("\t")

		if (cut_length(sent_a) and cut_length(sent_b)):
			output_data.append([str(idx), sent_a.rstrip("."), sent_b.rstrip("."), str(sim)])
			idx += 1
		else:
			continue

	return output_data, idx


def gen_test_pairs(input_file, sim_file, idx):
	f_input = open(input_file).readlines()
	f_label = open(sim_file).readlines()
	output_data = []

	for i in range(len(f_input)):
		line = f_input[i]
		sim = f_label[i]
		sent_a, sent_b = line.rstrip("\n").split("\t")
		output_data.append([str(idx), sent_a.rstrip("."), sent_b.rstrip("."), str(sim)])
		idx += 1

	return output_data, idx

def gen_dev_pairs(input_data):
	devLine = random.sample(range(0, len(input_data)), 500)
	train_data  = []
	dev_data = []
	for i in range(len(input_data)):
		if i in devLine:
			dev_data.append(input_data[i])
		else:
			train_data.append(input_data[i])

	return train_data, dev_data


if __name__ == '__main__':
	# dump train and dev data
	train_dir_list = ["train/2012-train/", "train/2012-test/", "train/2013-test/"]
	idx = 0
	output_data = []
	for dir_name in train_dir_list:
		train_input_flist, train_sim_flist = extract_file_name(dir_name)
		for _in, _sim in zip(train_input_flist, train_sim_flist):
			part_train_data, idx = gen_train_pairs(_in, _sim, idx)
			output_data.extend(part_train_data)
	print(len(output_data))
	train_data, dev_data = gen_dev_pairs(output_data)
	dump("train/", train_data)
	dump("dev/", dev_data)

	# dump test_data
	test_dir = "test/2014-test/"
	test_input_flist, test_sim_flist = extract_file_name(test_dir)

	for _in, _sim in zip(test_input_flist, test_sim_flist):
		test_data, idx = gen_test_pairs(_in, _sim, idx)
		dump("test/", test_data)








