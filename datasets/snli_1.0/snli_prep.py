import random
import pandas as pd
import os

def dump(folder, data):
	print(folder)
	f_id = open(os.path.join(folder, "id.txt"), "a+")
	f_q1 = open(os.path.join(folder, "a.toks"), "a+")
	f_q2 = open(os.path.join(folder, "b.toks"), "a+")
	f_label = open(os.path.join(folder, "sim.txt"), "a+")

	for item in data:
		f_id.write(item[0] + "\n")
		f_q1.write(item[1] + "\n")
		f_q2.write(item[2] + "\n")
		f_label.write(item[3] + "\n")

def transfer_label(label_name):
	if label_name == "neutral":
		return "0"
	elif label_name == "entailment":
		return "1"
	elif label_name == "contradiction":
		return "2"

def write_out(fname, idx):
	f = pd.read_csv(fname, delimiter='\t')
	output_data = []
	question1 = f['sentence1'].tolist()
	question2 = f['sentence2'].tolist()
	label = f['gold_label'].tolist()

	for q1, q2, lab in zip(question1, question2, label):
		try:
			if lab != "-":
				lab_to_num = transfer_label(lab)
				output_data.append([str(idx), q1[:-1], q2[:-1], lab_to_num])
				idx += 1
		except:
			print(q1, q2, lab)


	return output_data, idx

if __name__ == '__main__':
	train_file = "snli_1.0_train.txt"
	dev_file = "snli_1.0_dev.txt"
	test_file = "snli_1.0_test.txt"

	idx = 0
	train_data, idx = write_out(train_file, idx)
	dev_data, idx = write_out(dev_file, idx)
	test_data, idx = write_out(test_file, idx)

	print("finish loading data")
	dump("dev", dev_data)
	print("finish dev")
	dump("test", test_data)
	print("finish test")
	dump("train", train_data)
	print("finish train")


