This dataset is from https://languagenet.github.io/ which is released for non-commercial use under the CC BY-NC-SA 3.0 license.

## BUILD Twitter-URL Dataset

Run the following scripts to process the raw data into the Castor format.

```
mkdir train
mkdir test
python process.py
```