import random
import pandas as pd
import os


def split_dataset(fname):

        # Data frame : id qid1 qid2 question1  question2 is_duplicate
        f = pd.read_csv(fname, delimiter=',')

        lineTotal = len(f)

        # Dev set size: 10000
        # Test set size: 10000
        randomNumber = random.sample(range(0, lineTotal), 20000)
        lineDev = randomNumber[:10000]
        lineTest = randomNumber[10000:]

        return f, lineDev, lineTest


def dump(folder, data):
        print(folder)
        f_id = open(os.path.join(folder, "id.txt"), "a+")
        f_q1 = open(os.path.join(folder, "a.toks"), "a+")
        f_q2 = open(os.path.join(folder, "b.toks"), "a+")
        f_label = open(os.path.join(folder, "sim.txt"), "a+")
        
        for item in data:
                f_id.write(item[0] + "\n")
                f_q1.write(item[1] + "\n")
                f_q2.write(item[2] + "\n")
                f_label.write(item[3] + "\n")


def write_out(f, lineDev, lineTest):

        train_data = []
        dev_data = []
        test_data = []

        pair_id = f['id'].tolist()
        question1 = f['question1'].tolist()
        question2 = f['question2'].tolist()
        label = f['is_duplicate'].tolist()


        for idx in range(len(f)):
                try:             
                        if idx in lineDev:
                                dev_data.append([str(pair_id[idx]), question1[idx][:-1], question2[idx][:-1], str(label[idx])])
                        elif idx in lineTest:
                                test_data.append([str(pair_id[idx]), question1[idx][:-1], question2[idx][:-1], str(label[idx])])
                        else:
                                train_data.append([str(pair_id[idx]), question1[idx][:-1], question2[idx][:-1], str(label[idx])])
                except:
                        print(idx)

        print("finish loading data")
        dump("dev", dev_data)
        print("finish dev")
        dump("test", test_data)
        print("finish test")
        dump("train", train_data)
        print("finish train")


if __name__ == "__main__":
        file, lineDev, lineTest = split_dataset("questions.csv")
        write_out(file, lineDev, lineTest)









